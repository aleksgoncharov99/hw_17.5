#include <iostream>

class Vector 
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double Modulus()
	{
		double a;
		a = sqrt(x*x + y*y + z*z);
		return a;
	}
private:
	double x;
	double y;
	double z;
};

class Input
{
private:
	int x = 10;
	float y = 3.5;
	char z = 'A';

public:
	void ShowInput()
	{
		std::cout << x << ' ' << y << ' ' << z;
	}
	
};

int main()
{
	std::cout << "First Task:\n";
	Input a;
	a.ShowInput(); 
	std::cout << "\nSecond Task:\n";
	Vector n(2, 4, 4);
	std::cout << n.Modulus();
}
